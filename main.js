const electron = require('electron')
const fs = require('fs')
const readline = require('readline');
const {
  ipcMain
} = require('electron');

//https://electronjs.org/docs/tutorial/application-architecture
const {
  app,
  BrowserWindow
} = require('electron')

const net = require('net');
// var dgram = require('dgram');
// var server = dgram.createSocket('udp4');

let win = null;
// let win2 = null;

let client = null;
let serial = null;

var polyline_rl;
var point_rl;
var pwr_rl;

function createWindow() {
  win = new BrowserWindow({
    webPreferences: {
      nodeIntegration: true
    },
    width: 1250,
    height: 1050,
    x:0,
    y:0
  });

  win.loadFile('index.html');
  // win.webContents.openDevTools()
  win.setTitle("kt demo program.");
  // win.maximize();
  // win.setAspectRatio(16/9);
  win.focus();

  //read poly line.
  polyline_rl = readline.createInterface({
    input: fs.createReadStream('Reduce_A1_LANE.csv'),
    output: null
  });
  polyline_rl.pause();

  polyline_rl.on('line', (data) => {
  polyline_rl.pause();
    //parsing data to latlngs;
    let latlngs = [];
    let parsed_data = data.split(',');

    for (let i = 0; i < (parsed_data.length) / 2; i++) {
      let tmp = {};
      tmp.lat = parsed_data[2 * i];
      tmp.lng = parsed_data[2 * i + 1];
      latlngs.push(tmp);
    }
    win.webContents.send('polyline_data', latlngs);
  });
  ipcMain.on('polyline_start', (event, arg) => {
    // console.log('polyline_start');
    polyline_rl.resume();
  });


	// polyline_rl.on('close', () => {
	// 	point_rl.resume();
	// });
  // //read point.
  // point_rl = readline.createInterface({
  //   input: fs.createReadStream('.csv'),
  //   output: null
  // });
  // point_rl.pause();
	//
  // point_rl.on('line', (data) => {
  //   point_rl.pause();
	//
	// 	var latlng = data.split(',');
  //   win.webContents.send('point_data', latlng);
  // });
	//
  // ipcMain.on('point_start', (event, arg) => {
  //   point_rl.resume();
  // });

  win.on('closed', () => {
    // rl.close();
    app.quit()
    win = null
  })

  create_client();
};

function create_client() {
  // udp
  // server.on('listening',function(){
  //   var address = server.address();
  //   console.log('UDP server listening on ' + address.address);
  // });
  // server.on('message',function(message,remote){
  //   console.log(remote.address + ":" + message);
  // })
  // server.bind(9001,"192.168.0.55");
  if(serial == null){
    console.log("serial open try");
    var read_stream = fs.createReadStream('//./COM7');
    var read_com = readline.createInterface({
      input: read_stream,
      output: null
    });

    read_com.on('line',(data) => {
      console.log("data : " + data);
      let array = data.split(';').join(',').split(',');
      if (array.length < 6) {
        console.log("Too short data received.\n data : " + data);
        return;
      }
      if(win){
        win.webContents.send('pwr_data', array);
      }
    })
  }


  // if (client == null) {
  //   client = net.connect({
  //     port: 3001,
  //     host: "192.168.0.54"
  //   }, function(socket) { //function(socket) readline?
  //     // console.log(so);
  //     client.setEncoding('utf8');
  //     console.log("Power Pack connected.");
  //
  //     client.write("fileconfig open\n");
  //     client.write("log file bestposb ontime 1\n");
  //     client.write("log file gpgga ontime 1\n");
  //     client.write("log file inspvaxb ontime 1\n");
  //     client.write("LOG FILE RAWEPHEMB ONNEW\n \
  //                   LOG FILE GLOEPHEMERISB ONNEW\n \
  //                   LOG FILE BDSEPHEMERISB ONNEW\n \
  //                   LOG FILE GALEPHEMERISB ONNEW\n \
  //                   LOG FILE QZSSEPHEMERISB ONNEW\n \
  //                   LOG FILE HEADING2B ONNEW\n \
  //                   LOG FILE VERSIONB ONCE\n \
  //                   LOG FILE RXSTATUSB ONCHANGED\n \
  //                   LOG FILE RXCONFIGB ONCE\n \
  //                   LOG FILE RANGECMPB ONTIME 1\n \
  //                   LOG FILE BESTPOSB ONTIME 1\n \
  //                   LOG FILE BESTGNSSPOSB ONTIME 1\n \
  //                   LOG FILE RAWIMUSXB ONNEW\n \
  //                   LOG FILE INSPVAXB ONTIME 1\n \
  //                   LOG FILE INSUPDATESTATUSB ONNEW\n \
  //                   LOG FILE INSCONFIGB ONCHANGED\n \
  //                   LOG FILE TIMEDWHEELDATAB ONNEW\n \
  //                   LOG FILE SETWHEELPARAMETERSB ONCHANGED\n");
  //
  //     pwr_rl = readline.createInterface({
  //       input: client
  //       ,output: null
  //     });
  //
  //     pwr_rl.on('line', (data) => {
  //       // console.log("pwr data received. : " + data);
  //       let array = data.split(';').join(',').split(',');
  //       if (array.length < 6) {
  //         console.log("Too short data received.\n data : " + data);
  //         return;
  //       }
  //       if(win){
  //         win.webContents.send('pwr_data', array);
  //       }
  //     });
  //   });
  //   //
  //   client.on('error', function(data) {
  //     //send disconnected message to browser
  //     console.log('pwr_error');
  //     // client = null;
  //   });
  //   //
  //   client.on('end', function(data) {
  //     //send disconnected message to browser
  //     console.log('pwr_end.');
  //     client = null;
  //   });
  // }
}



app.on('ready', createWindow)
app.on('window-all-closed', () => {
  // if(client){
  //   client.write("fileconfig close\n");
  // }
  app.quit();
});

app.on('activate', () => {
  if (win === null) {
    createWindow();
  }
  // if(win2 ===null){
  //   createWindow2();
  // }
});
